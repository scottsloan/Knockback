import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

Item{
    id: playerScore
    height:100
    width:100

    property alias playerName: nameLabel.text
    property alias playerScore: scoreLabel.text

    Rectangle{
        anchors.fill: parent
        color:"black"
    }

    ColumnLayout{
        anchors.fill: parent

        Label {
            id: nameLabel
            text: qsTr("")
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            font.pointSize: 12
            font.bold: true
            color: "white"
        }

        Label{
            id: scoreLabel
            text: qsTr("0")
            fontSizeMode: Text.FixedSize
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            font.pointSize: 28
            color: "white"
        }
    }
}
