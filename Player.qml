import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: player
    property alias playerName: playerNameText.text
    property alias playerImage: playerSprite.source
    property alias healthPts : healthBar.value

    width: 60
    height: 250
    x : 0
    y: 0

    ColumnLayout{
        anchors.fill: parent

        Text {
            id: playerNameText
            text: qsTr("Steve")
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            color: "white"

        }

        ProgressBar {
            id : healthBar
            height: 10
            clip: true
            Layout.fillWidth: true
            to: 100
            value: 50

            background: Rectangle{
                anchors.fill: healthBar
                color: "transparent"
                radius: 3
                border.color: "black"
                border.width: 1
            }

            contentItem: Rectangle{
                anchors.left: healthBar.left
                anchors.verticalCenter: healthBar.verticalCenter
                color: "lightgreen"
                radius: 2
                height: parent.height
                width: healthBar.visualPosition * parent.width
            }
        }

        Image {
            id : playerSprite
            Layout.fillHeight: true
            source : "images/Steve.png"
        }
    }
}
