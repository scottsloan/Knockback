// Game Engine

function newGame() {
    resetPlayer();
    loadLevel(1);
}

function gameOver()
{
    gameView.currentLevel = "Gameover.qml"
}

function levelComplete()
{
    gameView.currentLevel = "LevelComplete.qml"

}

function victory()
{
    gameView.currentLevel = "Victory.qml"
}

function nextLevel()
{
    var newLevel = gameView.activeLevel + 1;

    if(newLevel > 3){
        victory();
        return;
    }

    loadLevel(newLevel);

}

function loadLevel(newLevel)
{
    var levelPath = "Levels/Level"+ newLevel.toString() + ".qml";
    var levelComp = Qt.createComponent(levelPath);

    if(levelComp === null){
        console.error("Unable to load level: "+ levelPath);
        return;
    }

    gameView.activeLevel = parseInt(newLevel,10);
    gameView.currentLevel = levelPath
    resetMob();
}

function checkVictoryDefeat()
{
    if(playerHealth <= 0)
        gameOver();

    if(mobHealth <= 0){
        if(gameView.activeLevel == 3)
            victory();
        else
            levelComplete();
    }
}


// Player
var playerName = "Steve";
var playerHealth = 100;

function resetPlayer()
{
    playerHealth = 100;
}

function dealHitPoints(hitPoints)
{
    playerHealth -= 10;
    player1.healthPts = playerHealth;

    checkVictoryDefeat();
}

// Mob

var mobHealth = 100;
var mobName = "Zombie"

function moveMob(deltaX)
{
    positionAnim.running = false;
    mob.x = mob.x + deltaX;
    positionAnim.running = true;

    if(mob.x <= 0 + player1.width)
    {
       dealHitPoints(10);
    }
}

function resetMob()
{
    switch(gameView.activeLevel)
    {
        case 1:
            mobHealth = 100;
            break;
        case 2:
            mobHealth = 500;
            break;
        case 3:
            mobHealth = 1000;
            break;
    }
}

function handleMobClick()
{
    mobHealth -= 10;
    mob.healthPts = mobHealth
    checkVictoryDefeat();
}
