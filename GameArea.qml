import QtQuick 2.0

import "KnockBack.js" as KnockBackGame

Item {
    id : gameCanvas

    property alias currentLevel: curLevel.source
    property int activeLevel : 1
    Loader{
        id: curLevel
        anchors.fill: parent
        source: "HomeView.qml"
        onLoaded: {
            item.width = parent.width
            item.height = parent.height
        }
    }
}
