import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "KnockBack.js" as KnockBackGame

Item {
    id : homeView

    Rectangle{
        anchors.fill: parent
        color : "green"
    }
    ColumnLayout{
        anchors.fill: parent
        Image {
            id: titleImg
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            transformOrigin: Item.Center
            source: "images/Title.png"
        }

        Button {
            id : playButton
            text: qsTr("Play")
            font.family: "Courier"
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            Layout.fillWidth: false
            font.capitalization: Font.AllUppercase
            onClicked: KnockBackGame.newGame()
        }
    }
}
