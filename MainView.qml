import QtQuick 2.9
import QtQuick.Window 2.3

Window {
    id: mainWindow
    visible: true
    width: 1024
    height: 768
    title: qsTr("Knockback")

    GameArea{
        id : gameView
        anchors.fill: parent
    }
}
