import QtQuick 2.0
import QtQuick.Controls 2.2

import "KnockBack.js" as KnockBackGame

Item {
    id : gameOverView

    Rectangle{
        anchors.fill: parent
        color: "#ff6363"
    }

    Button {
        id : respawnButton
        text: qsTr("Next Level")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        onClicked: KnockBackGame.nextLevel()
    }
}
