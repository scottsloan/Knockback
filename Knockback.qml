import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

Window {
    id: mainWindow
    visible: true
    width: 1024
    height: 768
    title: qsTr("Knockback")

    Rectangle{
        id : sky
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "blue" }
            GradientStop { position: 1.0; color: "lightblue" }
        }
    }

    RowLayout{
        id : scorebar
        height: 100
        anchors.right: parent.left
        anchors.rightMargin: mainWindow.width * -1
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        PlayerScore {
            id: steveScore
            height: parent.height
            width:parent.height
            playerName: qsTr("Steve")
            playerScore: "0"
        }

        PlayerScore {
            id: mobsScore
            height:parent.height
            width:parent.height
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            playerName: qsTr("Mobs")
            playerScore: "0"
        }
    }

    Image{
        id : ground
        width: parent.width
        anchors.bottom: parent.bottom
        source : "images/Grass_Block.png"
        anchors.bottomMargin: 0
        height:72
        fillMode: Image.TileHorizontally
        verticalAlignment: Image.AlignLeft
    }

    Image {
        id : steve
        x : 0
        y : parent.height - 288
        source : "images/Steve.png"
    }

    Image{
        id : mob
        source: "images/Zombie.png"
        x: 1024
        y: parent.height - 288

        MouseArea {
            anchors.fill: parent
            onClicked: {
                steveScore.playerScore =  parseInt(steveScore.playerScore) + 1;
                positionAnim.running = false;
                parent.x = parent.x + 100;
                positionAnim.running = true;
            }
        }

        NumberAnimation on x {
            running: true
            id: positionAnim
            to: 0
            duration: 6000
        }
    }
}
