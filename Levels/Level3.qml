import QtQuick 2.0

BaseLevel{
    groundImage: "../images/End_Block.png"
    mobImage: "../images/EnderDragon.png"
    mobY: 200
    mobName: "Ender Dragon"

    backgroundGradient: Gradient {
        GradientStop { position: 0.0; color: "Black" }
        GradientStop { position: 1.0; color: "#afafaf" }
    }
}
