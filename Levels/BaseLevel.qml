import QtQuick 2.0

import "../"
import "../KnockBack.js" as KnockBackGame

Rectangle {
    id : level

    property alias groundImage: ground.source
    property alias mobImage: mob.playerImage
    property alias mobY: mob.y
    property alias mobName: mob.playerName
    property alias backgroundGradient: backdrop.gradient;

    Rectangle{
        id : backdrop
        anchors.fill: parent
    }

    Image{
        id : ground
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        height:72
        fillMode: Image.TileHorizontally
        verticalAlignment: Image.AlignLeft
    }

    Player {
        id : player1
        x: 0
        y: parent.height - player1.height - ground.height
        playerName: KnockBackGame.playerName
        healthPts : KnockBackGame.playerHealth
        playerImage: "../images/Steve.png"
    }

    Player{
        id : mob
        x : 1024
        y : parent.height - mob.height - ground.height
        playerName: KnockBackGame.mobName
        healthPts: KnockBackGame.mobHealth
        onXChanged: {
            if(x <= 0 + player1.width)
            {
               KnockBackGame.dealHitPoints(10);
               KnockBackGame.moveMob(1);
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                KnockBackGame.handleMobClick();
                KnockBackGame.moveMob(100);
            }
        }

        NumberAnimation on x {
            running: true
            id: positionAnim
            to: 0
            duration: 6000
        }
    }
}
