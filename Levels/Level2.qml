import QtQuick 2.0

BaseLevel{
    groundImage: "../images/Netherrack_Block.png"
    mobImage: "../images/Ghast.png"
    mobName: "Ghast"

    backgroundGradient: Gradient {
        GradientStop { position: 0.0; color: "red" }
        GradientStop { position: 1.0; color: "#ff6363" }
    }
}
