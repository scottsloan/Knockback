import QtQuick 2.0

BaseLevel{
    groundImage: "../images/Grass_Block.png"
    mobImage: "../images/Zombie.png"
    mobName: "Zombie"

    backgroundGradient: Gradient {
        GradientStop { position: 0.0; color: "blue" }
        GradientStop { position: 1.0; color: "lightblue" }
    }
}
