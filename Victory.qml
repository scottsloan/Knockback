import QtQuick 2.0
import QtQuick.Controls 2.2

import "KnockBack.js" as KnockBackGame

Item {
    id : victoryView

    Rectangle{
        anchors.fill: parent
        color: "blue"
    }

    Button {
        id : respawnButton
        text: qsTr("You Won!")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        onClicked: KnockBackGame.newGame()
    }
}
